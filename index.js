var logger = require('winston');
var app = require('./app/index');

var port = 3000;
app.listen(port, function () {
	logger.info('Listening on port ' + port + '...');
});