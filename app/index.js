/*jslint node: true */

'use strict';
// index.js

var config = require('config');
var express = require('express');
var logger = require('winston');

var app = module.exports = express();

app.use(function(request, response, next) {
	//logger.info('VSG Hello World captured request to ' + request);
	next();
});

try {
	require('./routes')(app, logger);
}
catch (e) {
	logger.error(e.message);
}