/*jslint node: true */

'use strict';
// filetest.js

module.exports = function(app, logger) {
	var fs = require('fs');

	app.get('/v1/writeTest', function(request, response) {
		logger.info('GET /v1/writeTest');
		
		response.type('text/plain');

		fs.writeFile('testfile', new Date(), 'utf8', function (err) {
			if (err) {
				response.statusCode = 500;
				response.send('An error occurred while processing the write: ' + err);
			}
			else {
				response.statusCode = 200;
				response.send('File updated!');
			}
		});	
	});

	app.get('/v1/readTest', function(request, response) {
		logger.info('GET /v1/readTest');
		
		response.type('text/plain');

		fs.readFile('testfile', 'utf8', function (err, data) {
			if (err) {
				response.statusCode = 500;
				response.send('An error occurred while reading the file: ' + err);
			}
			else {
				response.statusCode = 200;
				response.send(data);
			}
		});

		
	});
};