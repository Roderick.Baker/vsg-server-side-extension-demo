/*jslint node: true */

'use strict';
// helloworld.js

module.exports = function(app, logger) {
	app.get('/v1/vsgHelloWorld', function(request, response) {
		logger.info('GET /v1/vsgHelloWorld');

		response.type('text/plain');
		response.statusCode = 200;
		response.send('Hello, VSG Commerce! /v1');
	});

	app.post('/v1/vsgHelloWorld', function(request, response) {
		logger.info('POST /v1/vsgHelloWorld');

		response.json({"request" : request});
		response.statusCode = 200;
	});
};